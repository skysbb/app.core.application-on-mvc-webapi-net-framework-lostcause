﻿
namespace App.Core.Shared.Models.Messages
{
    using System;
    using App.Core.Shared.Factories;

    public class ConfigurationStep
    {
        public ConfigurationStep()
        {
            this.Id = GuidFactory.NewGuid();
        }
        public Guid Id { get; set; }
        public ConfigurationStepType Type { get; set; }
        public ConfigurationStepStatus Status { get; set; }
        public DateTimeOffset DateTime { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
