﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Shared.Models.Messages
{
    using System.IO;

    public class UploadedMedia
    {
        public long Length { get; set; }
        public byte[] Stream { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }
}
