﻿namespace App.Core.Shared.Models
{
    public interface IHasText
    {
        string Text { get; set; }
    }
}