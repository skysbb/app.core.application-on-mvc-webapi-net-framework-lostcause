﻿namespace App.Core.Shared.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    // It's *not* called User because a security Principal can be a User, but also a Device or Service.
    public class Principal : UntenantedTimestampedAuditedRecordStatedGuidIdEntityBase, IHasEnabled
    {
        private ICollection<PrincipalClaim> _claims;
        private ICollection<PrincipalProperty> _properties;
        private ICollection<Role> _roles;

        public virtual NZDataClassification? DataClassificationFK { get; set; }
        public virtual DataClassification DataClassification { get; set; }

        public virtual PrincipalCategory Category { get; set; }
        public virtual Guid CategoryFK { get; set; }


        // The Application DisplayName
        public virtual string DisplayName { get; set; }


        public virtual ICollection<Role> Roles
        {
            get
            {
                if (this._roles == null)
                {
                    this._roles = new Collection<Role>();
                }
                return this._roles;
            }
            set => this._roles = value;
        }

        public virtual ICollection<PrincipalProperty> Properties
        {
            get
            {
                if (this._properties == null)
                {
                    this._properties = new Collection<PrincipalProperty>();
                }
                return this._properties;
            }
            set => this._properties = value;
        }

        public virtual ICollection<PrincipalClaim> Claims
        {
            get
            {
                if (this._claims == null)
                {
                    this._claims = new Collection<PrincipalClaim>();
                }
                return this._claims;
            }
            set => this._claims = value;
        }

        public virtual bool Enabled { get; set; }
    }
}