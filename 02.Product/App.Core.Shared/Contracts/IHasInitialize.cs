﻿namespace App.Core.Shared.Contracts
{
    // Contract for methods that have an Initialize() method.
    public interface IHasInitialize
    {
        void Initialize();
    }
}