namespace App.Core.Shared.Contracts
{
    public interface IHasSingletonLifecycle : IHasLifecycle
    {
    }
}