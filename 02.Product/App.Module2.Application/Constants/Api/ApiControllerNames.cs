﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Module2.Application.Constants.Api
{
    public static class ApiControllerNames
    {
        public static string Body = "bodydto";
        public static string EducationOrganisation = "educationorganisationdto";
    }
}
