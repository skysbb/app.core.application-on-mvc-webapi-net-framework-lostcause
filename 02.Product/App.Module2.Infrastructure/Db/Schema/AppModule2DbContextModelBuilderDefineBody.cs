﻿namespace App.Module2.DbContextModelBuilder
{
    using System.Data.Entity;
    using App.Core.Infrastructure.Db.Schema.Conventions;
    using App.Module2.Infrastructure.Initialization;
    using App.Module2.Infrastructure.Initialization.Db;
    using App.Module2.Shared.Models.Entities;

    public class AppModule2DbContextModelBuilderDefineBody : IHasAppModule2DbContextModelBuilderInitializer
    {
        public void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            new TenantFKEtcConvention().Define<Body>(modelBuilder, ref order);


            modelBuilder.Entity<Body>()
                .Property(x => x.Type)
                .HasColumnOrder(order++)
                .IsRequired();
            modelBuilder.Entity<Body>()
                .HasRequired(x => x.Category)
                .WithMany()
                .HasForeignKey(x => x.CategoryFK);
            modelBuilder.Entity<Body>()
                .Property(x => x.Key)
                .HasColumnOrder(order++)
                .HasMaxLength(256)
                .IsOptional();
            modelBuilder.Entity<Body>()
                .Property(x => x.Description)
                .HasColumnOrder(order++)
                .HasMaxLength(256)
                .IsOptional();
            modelBuilder.Entity<Body>()
                .HasMany(x => x.Properties)
                .WithOptional()
                .HasForeignKey(x => x.OwnerFK);
            modelBuilder.Entity<Body>()
                .HasMany(x => x.Claims)
                .WithOptional()
                .HasForeignKey(x => x.OwnerFK);
            modelBuilder.Entity<Body>()
                .HasMany(x => x.Aliases)
                .WithOptional()
                .HasForeignKey(x => x.OwnerFK);
            modelBuilder.Entity<Body>()
                .HasMany(x => x.Channels)
                .WithOptional()
                .HasForeignKey(x => x.OwnerFK);
            modelBuilder.Entity<Body>()
                .Property(x => x.Notes)
                .HasColumnOrder(order++)
                .HasMaxLength(2048)
                .IsOptional();
        }
    }
}