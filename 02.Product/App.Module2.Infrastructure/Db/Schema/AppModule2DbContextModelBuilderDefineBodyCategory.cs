﻿namespace App.Module2.DbContextModelBuilder
{
    using System.Data.Entity;
    using App.Core.Infrastructure.Db.Schema.Conventions;
    using App.Module2.Infrastructure.Initialization;
    using App.Module2.Infrastructure.Initialization.Db;
    using App.Module2.Shared.Models.Entities;

    public class AppModule2DbContextModelBuilderDefineBodyCategory : IHasAppModule2DbContextModelBuilderInitializer
    {
        public void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;


            new TenantFKEtcConvention().Define<BodyCategory>(modelBuilder, ref order);

            modelBuilder.Entity<BodyCategory>()
                .Property(x => x.Text)
                .HasColumnOrder(order++)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}