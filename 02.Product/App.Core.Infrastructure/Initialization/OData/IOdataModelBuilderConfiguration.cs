﻿
namespace App.Core.Infrastructure.Initialization.OData
{

    public interface IOdataModelBuilderConfigurationBase
    {
        void Define(/*ODataModelBuilder*/ object builder);
    }
}