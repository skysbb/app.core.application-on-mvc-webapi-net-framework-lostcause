﻿namespace App.Core.Infrastructure.Db.Schema
{
    using System.Data.Entity;
    using App.Core.Infrastructure.Db.Schema.Conventions;
    using App.Core.Infrastructure.Initialization.Db;
    using App.Core.Shared.Models.Entities;

    public class AppCoreDbContextModelBuilderDefinePrincipalCategory : IHasAppCoreDbContextModelBuilderInitializer
    {
        public void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;


            new NonTenantFKEtcConvention().Define<PrincipalCategory>(modelBuilder, ref order);

            modelBuilder.Entity<PrincipalCategory>()
                .Property(x => x.Text)
                .HasColumnOrder(order++)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}