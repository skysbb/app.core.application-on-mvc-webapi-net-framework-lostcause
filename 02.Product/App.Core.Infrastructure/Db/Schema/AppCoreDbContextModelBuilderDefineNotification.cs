﻿namespace App.Core.Infrastructure.Db.Schema
{
    using System.Data.Entity;
    using App.Core.Infrastructure.Db.Schema.Conventions;
    using App.Core.Infrastructure.Initialization;
    using App.Core.Infrastructure.Initialization.Db;
    using App.Core.Shared.Models.Entities;

    // A single DbContext Entity model map, 
    // invoked via a Module's specific DbContext ModelBuilderOrchestrator
    public class AppCoreDbContextModelBuilderDefineNotification : IHasAppCoreDbContextModelBuilderInitializer
    {
        public void Define(DbModelBuilder modelBuilder)
        {
            var order = 1;

            new NonTenantFKEtcConvention().Define<Notification>(modelBuilder, ref order);


            modelBuilder.Entity<Notification>()
                .Property(x => x.Type)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Notification>()
                .Property(x => x.Level)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Notification>()
                .Ignore(x => x.IsRead);


            modelBuilder.Entity<Notification>()
                .Property(x => x.PrincipalFK)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Notification>()
                .Property(x => x.DateTimeCreatedUtc)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Notification>()
                .Property(x => x.DateTimeReadUtc)
                .HasColumnOrder(order++)
                .IsOptional();


            modelBuilder.Entity<Notification>()
                .Property(x => x.From)
                .HasColumnOrder(order++)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Notification>()
                .Property(x => x.Class)
                .HasColumnOrder(order++)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Notification>()
                .Property(x => x.ImageUrl)
                .HasColumnOrder(order++)
                .HasMaxLength(50)
                .IsRequired();


            modelBuilder.Entity<Notification>()
                .Property(x => x.Value)
                .HasColumnOrder(order++)
                .IsRequired();

            modelBuilder.Entity<Notification>()
                .Property(x => x.Text)
                .HasColumnOrder(order++)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}