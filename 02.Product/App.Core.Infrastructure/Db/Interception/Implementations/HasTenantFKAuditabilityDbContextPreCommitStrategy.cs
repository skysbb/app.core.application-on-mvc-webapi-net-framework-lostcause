﻿namespace App.Core.Infrastructure.Db.Interception.Implementations
{
    using System;
    using System.Data.Entity;
    using App.Core.Infrastructure.Db.Interception.Implementations.Base;
    using App.Core.Infrastructure.Services;
    using App.Core.Shared.Models;

    public class
        HasTenantFKAuditabilityDbContextPreCommitStrategy : DbContextPreCommitProcessingStrategyBase<IHasTenantFK>
    {
        private readonly ITenantService _tenantService;

        public HasTenantFKAuditabilityDbContextPreCommitStrategy(IUniversalDateTimeService dateTimeService,
            IPrincipalService principalService, ITenantService tenantService) : base(dateTimeService, principalService,
            EntityState.Added)
        {
            this._tenantService = tenantService;
        }

        protected override void PreProcessEntity(IHasTenantFK entity)
        {
            if (entity.TenantFK.Equals(Guid.Empty))
            {
                entity.TenantFK = this._tenantService.PrincipalTenant.Id;
            }
        }
    }
}