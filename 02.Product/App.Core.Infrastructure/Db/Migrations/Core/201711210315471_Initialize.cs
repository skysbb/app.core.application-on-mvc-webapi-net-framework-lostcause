namespace App.Core.Infrastructure.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Core.MediaMetadatas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        UploadedDateTimeUtc = c.DateTime(nullable: false),
                        ContentHash = c.String(nullable: false),
                        ContentSize = c.Long(nullable: false),
                        MimeType = c.String(nullable: false),
                        SourceFileName = c.String(nullable: false),
                        LatestScanDateTimeUtc = c.DateTime(),
                        LatestScanResults = c.String(),
                        LatestScanMalwareDetetected = c.Boolean(),
                        LocalName = c.String(nullable: false),
                        DataClassificationFK = c.Int(nullable: false),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.DataClassifications", t => t.DataClassificationFK, cascadeDelete: true)
                .Index(t => t.DataClassificationFK);
            
            CreateTable(
                "Core.DataClassifications",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        Enabled = c.Boolean(nullable: false),
                        Text = c.String(nullable: false),
                        DisplayStyleHint = c.String(),
                        DisplayOrderHint = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Core.Notifications",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        Type = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                        PrincipalFK = c.Guid(nullable: false),
                        DateTimeCreatedUtc = c.DateTime(nullable: false),
                        DateTimeReadUtc = c.DateTime(),
                        From = c.String(nullable: false, maxLength: 50),
                        Class = c.String(nullable: false, maxLength: 50),
                        ImageUrl = c.String(nullable: false, maxLength: 50),
                        Value = c.Int(nullable: false),
                        Text = c.String(nullable: false, maxLength: 50),
                        TenantFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Core.SessionOperations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        BeginDateTimeUtc = c.DateTimeOffset(nullable: false, precision: 7),
                        EndDateTimeUtc = c.DateTimeOffset(nullable: false, precision: 7),
                        Duration = c.Time(nullable: false, precision: 7),
                        ClientIp = c.String(nullable: false, maxLength: 50),
                        Url = c.String(nullable: false, maxLength: 50),
                        ControllerName = c.String(nullable: false, maxLength: 50),
                        ActionName = c.String(nullable: false, maxLength: 50),
                        ActionArguments = c.String(nullable: false),
                        ResponseCode = c.String(nullable: false, maxLength: 50),
                        OwnerFK = c.Guid(nullable: false),
                        ResourceTenantKey = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.Sessions", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.ControllerName, unique: true, name: "IX_SessionOperationLog_ControllerName")
                .Index(t => t.ActionName, unique: true, name: "IX_SessionOperationLog_ActionName")
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "Core.Sessions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        Enabled = c.Boolean(nullable: false),
                        PrincipalFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.Principals", t => t.PrincipalFK, cascadeDelete: true)
                .Index(t => t.PrincipalFK);
            
            CreateTable(
                "Core.Principals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        DisplayName = c.String(nullable: false, maxLength: 50),
                        DataClassificationFK = c.Int(),
                        CategoryFK = c.Guid(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.PrincipalCategories", t => t.CategoryFK, cascadeDelete: true)
                .ForeignKey("Core.DataClassifications", t => t.DataClassificationFK)
                .Index(t => t.DataClassificationFK)
                .Index(t => t.CategoryFK);
            
            CreateTable(
                "Core.PrincipalCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        Text = c.String(nullable: false, maxLength: 50),
                        Enabled = c.Boolean(nullable: false),
                        DisplayOrderHint = c.Int(nullable: false),
                        DisplayStyleHint = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Core.PrincipalClaims",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        OwnerFK = c.Guid(nullable: false),
                        Authority = c.String(nullable: false, maxLength: 50),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                        AuthoritySignature = c.String(nullable: false, maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.Principals", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "Core.PrincipalProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        OwnerFK = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.Principals", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "Core.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        Key = c.String(nullable: false, maxLength: 50),
                        Enabled = c.Boolean(nullable: false),
                        ModuleKey = c.String(),
                        DataClassificationFK = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.DataClassifications", t => t.DataClassificationFK)
                .Index(t => t.DataClassificationFK);
            
            CreateTable(
                "Core.Tenants",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        IsDefault = c.Boolean(),
                        Enabled = c.Boolean(nullable: false),
                        Key = c.String(nullable: false, maxLength: 50),
                        HostName = c.String(maxLength: 50),
                        DisplayName = c.String(nullable: false, maxLength: 50),
                        DataClassificationFK = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.DataClassifications", t => t.DataClassificationFK)
                .Index(t => t.IsDefault, unique: true, name: "IX_Tenant_IsDefault")
                .Index(t => t.Key, unique: true, name: "IX_Tenant_Key")
                .Index(t => t.HostName, unique: true, name: "IX_Tenant_HostName")
                .Index(t => t.DataClassificationFK);
            
            CreateTable(
                "Core.TenantClaims",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        OwnerFK = c.Guid(nullable: false),
                        Authority = c.String(nullable: false, maxLength: 50),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                        AuthoritySignature = c.String(nullable: false, maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.Tenants", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "Core.TenantProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        OwnerFK = c.Guid(nullable: false),
                        Key = c.String(nullable: false, maxLength: 50),
                        Value = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Core.Tenants", t => t.OwnerFK, cascadeDelete: true)
                .Index(t => t.OwnerFK);
            
            CreateTable(
                "Core.DataTokens",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RecordState = c.Int(nullable: false),
                        CreatedOnUtc = c.DateTime(nullable: false),
                        CreatedByPrincipalId = c.String(nullable: false),
                        LastModifiedOnUtc = c.DateTime(nullable: false),
                        LastModifiedByPrincipalId = c.String(nullable: false),
                        DeletedOnUtc = c.DateTime(),
                        DeletedByPrincipalId = c.String(),
                        TenantFK = c.Guid(nullable: false),
                        Value = c.String(nullable: false, maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Core.Principal2Role",
                c => new
                    {
                        PrincipalFK = c.Guid(nullable: false),
                        RoleFK = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.PrincipalFK, t.RoleFK })
                .ForeignKey("Core.Principals", t => t.PrincipalFK, cascadeDelete: true)
                .ForeignKey("Core.Roles", t => t.RoleFK, cascadeDelete: true)
                .Index(t => t.PrincipalFK)
                .Index(t => t.RoleFK);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Core.TenantProperties", "OwnerFK", "Core.Tenants");
            DropForeignKey("Core.Tenants", "DataClassificationFK", "Core.DataClassifications");
            DropForeignKey("Core.TenantClaims", "OwnerFK", "Core.Tenants");
            DropForeignKey("Core.Sessions", "PrincipalFK", "Core.Principals");
            DropForeignKey("Core.Principal2Role", "RoleFK", "Core.Roles");
            DropForeignKey("Core.Principal2Role", "PrincipalFK", "Core.Principals");
            DropForeignKey("Core.Roles", "DataClassificationFK", "Core.DataClassifications");
            DropForeignKey("Core.PrincipalProperties", "OwnerFK", "Core.Principals");
            DropForeignKey("Core.Principals", "DataClassificationFK", "Core.DataClassifications");
            DropForeignKey("Core.PrincipalClaims", "OwnerFK", "Core.Principals");
            DropForeignKey("Core.Principals", "CategoryFK", "Core.PrincipalCategories");
            DropForeignKey("Core.SessionOperations", "OwnerFK", "Core.Sessions");
            DropForeignKey("Core.MediaMetadatas", "DataClassificationFK", "Core.DataClassifications");
            DropIndex("Core.Principal2Role", new[] { "RoleFK" });
            DropIndex("Core.Principal2Role", new[] { "PrincipalFK" });
            DropIndex("Core.TenantProperties", new[] { "OwnerFK" });
            DropIndex("Core.TenantClaims", new[] { "OwnerFK" });
            DropIndex("Core.Tenants", new[] { "DataClassificationFK" });
            DropIndex("Core.Tenants", "IX_Tenant_HostName");
            DropIndex("Core.Tenants", "IX_Tenant_Key");
            DropIndex("Core.Tenants", "IX_Tenant_IsDefault");
            DropIndex("Core.Roles", new[] { "DataClassificationFK" });
            DropIndex("Core.PrincipalProperties", new[] { "OwnerFK" });
            DropIndex("Core.PrincipalClaims", new[] { "OwnerFK" });
            DropIndex("Core.Principals", new[] { "CategoryFK" });
            DropIndex("Core.Principals", new[] { "DataClassificationFK" });
            DropIndex("Core.Sessions", new[] { "PrincipalFK" });
            DropIndex("Core.SessionOperations", new[] { "OwnerFK" });
            DropIndex("Core.SessionOperations", "IX_SessionOperationLog_ActionName");
            DropIndex("Core.SessionOperations", "IX_SessionOperationLog_ControllerName");
            DropIndex("Core.MediaMetadatas", new[] { "DataClassificationFK" });
            DropTable("Core.Principal2Role");
            DropTable("Core.DataTokens");
            DropTable("Core.TenantProperties");
            DropTable("Core.TenantClaims");
            DropTable("Core.Tenants");
            DropTable("Core.Roles");
            DropTable("Core.PrincipalProperties");
            DropTable("Core.PrincipalClaims");
            DropTable("Core.PrincipalCategories");
            DropTable("Core.Principals");
            DropTable("Core.Sessions");
            DropTable("Core.SessionOperations");
            DropTable("Core.Notifications");
            DropTable("Core.DataClassifications");
            DropTable("Core.MediaMetadatas");
        }
    }
}
