﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using System.Linq;
    using System.Security.Claims;

    public class PrincipalService : AppCoreServiceBase, IPrincipalService
    {
        // OWIN auth middleware constants
        public const string cultureElementId = "http://schemas.org.tld/identity/claims/culture";

        public const string principalKeyElementId = "http://schemas.org.tld/identity/claims/tenant";

        public const string scopeElementId = "http://schemas.microsoft.com/identity/claims/scope";
        public const string objectIdElementId = "http://schemas.microsoft.com/identity/claims/objectidentifier";

        public ClaimsIdentity CurrentIdentity => ClaimsPrincipal.Current.Identities.FirstOrDefault();


        public string CurrentName
        {
            get
            {
                //http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier (ie ID)
                // not 'Name' (ie: "SSmith").
                var signedInUserID = this.Current.FindFirst(ClaimTypes.NameIdentifier)?.Value;

                return signedInUserID;
            }
        }

        public ClaimsPrincipal Current => ClaimsPrincipal.Current;

        // Validate to ensure the necessary scopes are present.
        public bool HasRequiredScopes(string permission, string scopeElement = scopeElementId)
        {
            var value = this.Current?.FindFirst(scopeElement)?.Value?.Contains(permission);
            return value != null && value.Value;
        }

        public string CurrentPrincipalIdentifier
        {
            get
            {
                var owner = ClaimsPrincipal.Current.FindFirst(objectIdElementId)?.Value;
                return owner;
            }
        }

        public bool IsInRole(params string[] roles)
        {
            return roles.Any(x => ClaimsPrincipal.Current.IsInRole(x));
        }


        public string ClaimPreferredCultureCode
        {
            get => GetClaimValue(cultureElementId);
            set => SetClaimValue(cultureElementId, value);
        }

        /// <summary>
        ///     The Key of the Tenant of the current Security Principal (ie, the Tenant Key within a Claim of the Thread current
        ///     Principal)
        /// </summary>
        public string PrincipalTenantKey
        {
            get => GetClaimValue(principalKeyElementId);
            set => SetClaimValue(principalKeyElementId, value);
        }


        private string GetClaimValue(string claimName)
        {
            var cultureInfoCode = this.Current.FindFirst(claimName)?.Value;
            return cultureInfoCode;
        }

        private void SetClaimValue(string claimName, string value)
        {
            var claim = this.Current.FindFirst(claimName);
            if (claim != null)
            {
                this.CurrentIdentity
                    .TryRemoveClaim(claim);
            }
            claim = new Claim(claimName, value);
            this.CurrentIdentity.AddClaim(claim);
        }
    }
}