﻿namespace App.Core.Infrastructure.Services.Implementations
{
    using App.Core.Infrastructure.Constants;
    using App.Core.Infrastructure.Constants.Db;
    using App.Core.Shared.Models.Entities;

    public class SessionOperationLogService : ISessionOperationLogService
    {
        private readonly IRepositoryService _repositoryService;
        private SessionOperation _current;

        public SessionOperationLogService(IRepositoryService repositoryService)
        {
            this._repositoryService = repositoryService;
            // TODO: We should be working against a GenericRepositoryService rather than directly against a specific DbContext
        }

        public SessionOperation Current => this._current ?? (this._current = new SessionOperation());

        public void Log(SessionOperation sessionOperationLog)
        {
            this._repositoryService.AddOnCommit(AppCoreDbContextNames.Core, sessionOperationLog);
        }

        public void Log()
        {
            if (this._current == null)
            {
                return;
            }
            this._repositoryService.AddOnCommit(AppCoreDbContextNames.Core, this._current);
        }
    }
}