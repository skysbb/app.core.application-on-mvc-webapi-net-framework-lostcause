﻿namespace App.Core.Infrastructure.Services
{
    using System.Security.Claims;
    using App.Core.Shared.Services;

    public interface IPrincipalService : IHasAppCoreService
    {
        string CurrentName { get; }

        ClaimsPrincipal Current { get; }

        string CurrentPrincipalIdentifier { get; }

        string ClaimPreferredCultureCode { get; set; }

        string PrincipalTenantKey { get; set; }

        bool HasRequiredScopes(string permission,
            string scopeElement = "http://schemas.microsoft.com/identity/claims/scope");

        bool IsInRole(params string[] roles);
    }
}