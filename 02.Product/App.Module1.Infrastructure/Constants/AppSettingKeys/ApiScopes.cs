namespace App.Module1.Infrastructure.IDA.Constants.HostSettingsKeys
{
    public static class ApiScopes
    {
        public static string ApiExampleReadScope = "example:read";
        public static string ApiExampleWriteScope = "example:write";
    }
}