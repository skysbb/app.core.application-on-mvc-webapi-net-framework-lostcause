﻿namespace App.Core.Application.ServiceFacade.Controllers.API.V0100
{
    using System.Collections.Generic;
    using System.Web.Http;

    public class SmokeCheck
    {
        public int Id { get; set; }
        public string SomeLabel { get; set; }
    }

    /// <summary>
    ///     Example1 is a straight WebAPI Controller, with DI, but no backing Db (it's just an in-mem list 'db').
    /// </summary>
    //See how to do this *globally*: [ExampleWebApiActionFilter] [AppCoreDbContextWebApiFilter]
    public class Values1Controller : ApiController
    {
        private static readonly List<SmokeCheck> _inMemFakeDb = new List<SmokeCheck>();

        static Values1Controller()
        {
            _inMemFakeDb.Add(new SmokeCheck {Id = 1, SomeLabel = "Foo"});
            _inMemFakeDb.Add(new SmokeCheck {Id = 2, SomeLabel = "Bar"});
        }

        public IEnumerable<SmokeCheck> Get()
        {
            IEnumerable<SmokeCheck> item = _inMemFakeDb /*no filtering*/;
            return item;
        }
    }
}