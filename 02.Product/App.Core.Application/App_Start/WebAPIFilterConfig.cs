﻿namespace App.Core.Application.App_Start
{
    using System.Web.Http;
    using App.Core.Application.Filters.WebApi;
    using App.Core.Infrastructure.Services;
    using App.Core.Shared.Models.Messages;

    public static class WebApiFilterConfig
    {
        public static void RegisterGlobalFilters(HttpConfiguration httpConfiguration,
            ISessionOperationLogService sessionOperationLogService)
        {
            // Since WebAPI pipeline is totally separate from WebMVC, the registrarion
            // of the WebAPI interception is done in a different way.
            // WebAPI Controllers are registered using HttpConfiguration:

            var filters = httpConfiguration.Filters;

            filters.Add(new ExampleWebApiActionFilterAttribute(sessionOperationLogService));


            // Apply a custom Filter to intercept WebAPI requests and return errors (no redirection).
            filters.Add(new RequireHttpsWebApiFilterAttribute());
            AppDependencyLocator.Current.GetInstance<IConfigurationStepService>()
                .Register(
                    ConfigurationStepType.Security,
                    ConfigurationStepStatus.Green,
                    "HTTPS Required (WebAPI)",
                    "WebAPI Filter installed to redirect HTTP requests to HTTPS.");


            filters.Add(new AppCoreDbContextWebApiActionFilterAttribute());
            AppDependencyLocator.Current.GetInstance<IConfigurationStepService>()
                .Register(
                    ConfigurationStepType.General,
                    ConfigurationStepStatus.White,
                    "DbContext Commit at end of commands.",
                    "WebApi Filter installed to automatically commit all pending changes.");

            //NO: More securely done within Global.asax.cs: filters.Add(new ThrottleWebApiActionFilterAttribute());


        }
    }
}