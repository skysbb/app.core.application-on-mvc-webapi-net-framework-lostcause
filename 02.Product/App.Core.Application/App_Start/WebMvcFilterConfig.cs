﻿namespace App.Core.Application
{
    using System.Web.Mvc;
    using App.Core.Application.ErrorHandler;
    using App.Core.Application.Filters.WebApi;
    using App.Core.Application.Filters.WebMvc;
    using App.Core.Infrastructure.Services;
    using App.Core.Shared.Models.Messages;

    /// <summary>
    ///     Microsoft should maybe have called this MvcFilterConfig to ensure developers
    ///     did not think they could register WebAPI Filters here.
    ///     <para>
    ///         That said, do not rename it as when you add Azure App Insights it will be looking
    ///         for a class named 'FilterConfig' in order to add its Filter.
    ///         Not sure what it does if not found.
    ///     </para>
    /// </summary>
    public class WebMvcFilterConfig
    {
        // Register MVC (not WebAPI) filters:
        public static void RegisterWebMvcGlobalFilters(GlobalFilterCollection filters)
        {
            var usingWebActivator = false;

            filters.Add(new ExampleWebMvcActionFilterAttribute());

            filters.Add(new MyRequireHttpsWebMvcFilterAttribute());
            AppDependencyLocator.Current.GetInstance<IConfigurationStepService>()
                .Register(
                    ConfigurationStepType.Security,
                    ConfigurationStepStatus.Green,
                    "HTTPS Required (WebMVC)",
                    "Filter installed to redirect HTTP requests to HTTPS.");

            filters.Add(new AppCoreDbContextWebMvcActionFilterAttribute());
            AppDependencyLocator.Current.GetInstance<IConfigurationStepService>()
                .Register(
                    ConfigurationStepType.General,
                    ConfigurationStepStatus.White,
                    "DbContext Commit at end of commands.",
                    "WebApi Filter installed to automatically commit all pending changes.");
            
            //NO: More securely done within Global.asax.cs: filters.Add(new ThrottleMvcActionFilterAttribute());



            // Note that neither of the above cover all cases 
            // (eg: still static file and classic webform requests will get through without
            // more work done elsewhere (earlier pipeline interception and web server Url Redirect Rules).
            filters.Add(new AiHandleErrorAttribute());
        }
    }
}