﻿namespace App.Core.Application
{
    using System.Collections.Generic;
    using App.Core.Infrastructure.IDA.Models;
    using App.Core.Infrastructure.IDA.Models.Enums;
    using App.Core.Infrastructure.IDA.Owin;
    using App.Core.Infrastructure.Initialization;
    using App.Core.Infrastructure.Initialization.Authentication;
    using App.Core.Infrastructure.Services;
    using Owin;

    public class AuthConfig
    {
        public static void Configure(IAppBuilder appBuilder)
        {
            // No:
            //_diagnosticsTracingService = StructuremapMvc.StructureMapDependencyScope.Container.GetInstance<IDiagnosticsTracingService>();
            // _hostSettingsService =StructuremapMvc.StructureMapDependencyScope.Container.GetInstance<IHostSettingsService>();
            // Better (no direct reference to 3rd party vendor, allowing you how-swap DI service providers without change to code):
            var diagnosticsTracingService = AppDependencyLocator.Current.GetInstance<IDiagnosticsTracingService>();
            var hostSettingsService = AppDependencyLocator.Current.GetInstance<IHostSettingsService>();

            var scopes = new[] {"TODO"};

            var t = new List<string>();
            AppDependencyLocator.Current.GetAllInstances<IHasOidcScopeInitializer>()
                .ForEach(x => t.AddRange(x.FullyQualifiedScopeDefinitions));
            scopes = t.ToArray();

            var authorisationConfiguration = hostSettingsService.GetObject<AuthorisationConfiguration>();
            var demoType = authorisationConfiguration.AuthorisationDemoType;

            switch (demoType)
            {
                case AuthorisationDemoType.AADUsingOIDCAndCookies:
                    AADV2ForOIDCCookiesConfiguration.Configure(appBuilder, hostSettingsService);
                    break;
                case AuthorisationDemoType.B2CUsingOIDCAndCookies:
                    new B2COAuthCookieBasedAuthenticationConfig(diagnosticsTracingService, hostSettingsService)
                        .Configure(appBuilder, scopes);
                    break;
                case AuthorisationDemoType.B2CUsingOIDCAndBearerTokens:
                    new B2COAuthBearerTokenAuthenticationConfiguration(diagnosticsTracingService, hostSettingsService)
                        .Configure(appBuilder);
                    break;
                case AuthorisationDemoType.B2CUsingOIDCAndCookiesAndBearerTokens:
                    new B2COAuthBearerTokenAuthenticationConfiguration(diagnosticsTracingService, hostSettingsService)
                        .Configure(appBuilder);
                    new B2COAuthCookieBasedAuthenticationConfig(diagnosticsTracingService, hostSettingsService)
                        .Configure(appBuilder, scopes);
                    break;
            }
        }
    }
}