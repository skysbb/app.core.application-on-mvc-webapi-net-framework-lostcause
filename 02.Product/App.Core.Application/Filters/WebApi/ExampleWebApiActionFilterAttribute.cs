namespace App.Core.Application.Filters.WebApi
{
    using System;
    using System.Web;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;
    using App.Core.Infrastructure.Services;
    using App.Core.Shared.Models.Entities;
    using Newtonsoft.Json;

    /// <summary>
    ///     This example Filter will intercept all MVC calls.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class ExampleWebApiActionFilterAttribute : ActionFilterAttribute
    {
        private readonly ISessionOperationLogService _sessionOperationLogService;

        public ExampleWebApiActionFilterAttribute(ISessionOperationLogService sessionOperationLogService)
        {
            this._sessionOperationLogService = sessionOperationLogService;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            // Filters are shared across requests, so save variables within the Request context to  pass between methods:
            var sessionOperationLogService = /*_sessionOperationLogService*/
                AppDependencyLocator.Current.GetInstance<ISessionOperationLogService>();
            var sessionOperationLog = sessionOperationLogService.Current;
            sessionOperationLog.BeginDateTimeUtc = DateTime.UtcNow;
            sessionOperationLog.ClientIp = actionContext.Request.RemoteIPChain();
            sessionOperationLog.Url = actionContext.Request.RequestUri.OriginalString;
            sessionOperationLog.ControllerName = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            sessionOperationLog.ActionName = actionContext.ActionDescriptor.ActionName;
            sessionOperationLog.ActionArguments = JsonConvert.SerializeObject(actionContext.ActionArguments,
                Formatting.Indented, new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
            // HACK: for now until sorted out transient lifespan of services that are not injected...
            HttpContext.Current.Items["SessionOperationLog"] = sessionOperationLog;

            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);

            // The initial one injected during startup. The DbContext it in turn was injected with is...dead.
            // This approach is impossible until we develop a GenericRepositoryService that provides some
            // uncoupling from the Current underlying datacontext.
            //// So, although not the favorite approach, instead of using DI, use ServiceLocation:
            var sessionOperationLogService = /*_sessionOperationLogService*/
                AppDependencyLocator.Current.GetInstance<ISessionOperationLogService>();
            //var sessionOperationLog = sessionOperationLogService.Current;
            var sessionOperationLog = HttpContext.Current.Items["SessionOperationLog"] as SessionOperation;
            sessionOperationLog.EndDateTimeUtc = DateTime.UtcNow;
            sessionOperationLog.Duration =
                sessionOperationLog.EndDateTimeUtc.Subtract(sessionOperationLog.BeginDateTimeUtc);
            if (actionExecutedContext.Response != null)
            {
                sessionOperationLog.ResponseCode = ((int) actionExecutedContext.Response.StatusCode).ToString();
            }
            else
            {
                sessionOperationLog.ResponseCode = "-1";
            }
            //Now persist.
            sessionOperationLogService.Log(sessionOperationLog);
        }
    }
}