﻿namespace App.Core.Application.Filters.WebApi
{
    using System;
    using System.Web.Http.Filters;
    using App.Core.Infrastructure.Db.Context;

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AppCoreDbContextWebApiActionFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        ///     Occurs after the action method is invoked.
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            // Notes:
            // Being a System.Web.Http.Filters based, this Filter will only work for WebAPI/OData 
            // controller calls -- you need another filter based on the Mvc namespaces, in order 
            // to handle the MVC Controllers.
            // 
            // In case you are wondering -- don't need to override the async version
            // of this method (OnActionExecuted) as it will be invoking this method anyway.

            // Get Current DbContext
            var appCoreDbContext = AppDependencyLocator.Current.GetInstance<AppCoreDbContext>();

            var shouldSave = appCoreDbContext.ChangeTracker.HasChanges();

            if (shouldSave)
            {
                PreprocessModelsBeforeSaving();
                appCoreDbContext.SaveChanges();
                var check = true;
            }
            base.OnActionExecuted(actionExecutedContext);
        }

        private void PreprocessModelsBeforeSaving()
        {
            // Complete models (eg: fill in CurrentUser, CreateDateTimeUtc, fields, etc.)
        }
    }
}