﻿namespace App.Core.Application.Filters.WebMvc
{
    using System;
    using System.Web.Mvc;
    using App.Core.Infrastructure.Db.Context;

    /// <summary>
    ///     An Filter for MVC Controllers (not WebAPI)
    ///     that will ensure that the DbContext is committed at the end of the request --
    ///     but only if there is something to commit.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AppCoreDbContextWebMvcActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            //Get Current DbContext
            var appCoreDbContext = AppDependencyLocator.Current.GetInstance<AppCoreDbContext>();

            // And Commit as need be:
            var shouldSave = appCoreDbContext.ChangeTracker.HasChanges();
            if (shouldSave)
            {
                PreprocessModelsBeforeSaving();
                appCoreDbContext.SaveChanges();
            }

            base.OnResultExecuted(filterContext);
        }

        private void PreprocessModelsBeforeSaving()
        {
            // Complete models (eg: fill in CurrentUser, CreateDateTimeUtc, fields, etc.)
        }
    }
}