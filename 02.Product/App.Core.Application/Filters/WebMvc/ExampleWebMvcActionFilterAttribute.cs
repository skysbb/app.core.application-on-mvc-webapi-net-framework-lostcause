﻿namespace App.Core.Application.Filters.WebMvc
{
    using System;
    using System.Web.Mvc;
    using App.Core.Infrastructure.Services;
    using App.Core.Shared.Models.Entities;
    using Newtonsoft.Json;

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class ExampleWebMvcActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Filters are shared across requests, so save variables within the Request context to  pass between methods:


            var sessionOperationLogService = /*_sessionOperationLogService*/
                AppDependencyLocator.Current.GetInstance<ISessionOperationLogService>();
            var sessionOperationLog = sessionOperationLogService.Current;
            sessionOperationLog.BeginDateTimeUtc = DateTime.UtcNow;
            sessionOperationLog.ClientIp = filterContext.RequestContext.HttpContext.Request.RemoteIPChain();
            sessionOperationLog.Url = filterContext.RequestContext.HttpContext.Request.RawUrl;
            sessionOperationLog.ControllerName =
                filterContext.ActionDescriptor.ControllerDescriptor
                    .ControllerName; //filterContext.RouteData.Values["controller"] as string;
            sessionOperationLog.ActionName =
                filterContext.ActionDescriptor.ActionName; //filterContext.RouteData.Values["action"] as string;

            try
            {
                sessionOperationLog.ActionArguments = JsonConvert.SerializeObject(filterContext.ActionParameters,
                    Formatting.Indented,
                    new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
            }
            catch
            {
                sessionOperationLog.ActionArguments = "ERROR";
            }
        
            App.AppDependencyLocator.Current.GetInstance<IRepositoryService>().AddOnCommit(App.Core.Infrastructure.Constants.Db.AppCoreDbContextNames.Core,sessionOperationLog);
        // HACK: for now until sorted out transient lifespan of services that are not injected...
            filterContext.HttpContext.Items["SessionOperationLog"] = sessionOperationLog;
            //Measure name
            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);

            var sessionOperationLogService = /*_sessionOperationLogService*/
                AppDependencyLocator.Current.GetInstance<ISessionOperationLogService>();
            //var sessionOperationLog = sessionOperationLogService.Current;
            var sessionOperationLog = filterContext.HttpContext.Items["SessionOperationLog"] as SessionOperation;


            sessionOperationLog.EndDateTimeUtc = DateTimeOffset.UtcNow;
            sessionOperationLog.Duration =
                sessionOperationLog.EndDateTimeUtc.Subtract(sessionOperationLog.BeginDateTimeUtc);
            sessionOperationLog.ResponseCode = filterContext.HttpContext.Response.StatusCode.ToString();


            var msg = "Ivoked:";
            var diagnosticsTracingService =
                AppDependencyLocator.Current.GetInstance<IDiagnosticsTracingService>();
            //GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IDiagnosticsTracingService)) as IDiagnosticsTracingService;
            diagnosticsTracingService.Trace(TraceLevel.Info, msg);

            sessionOperationLogService.Log(sessionOperationLog);
        }
    }
}