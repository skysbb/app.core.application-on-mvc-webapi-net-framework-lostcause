﻿namespace App.Core.Application.Constants.Api
{


    public static class ApiControllerNames
    {
        public const string ConfigurationStep = "configurationstepdto";
        public const string Principal = "principaldto";
        public const string MediaMetadata = "mediametadatadto";
        public const string Notification = "notificationdto";
        public const string Session = "sessiondto";
        public const string Tennant = "tenantdto";
    }
}
