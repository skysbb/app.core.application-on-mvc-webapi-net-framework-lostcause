﻿namespace App.Module2.Shared.Models.Entities
{
    public enum BodyType
    {
        // An error state.
        Undefined = 0,
        Person = 1,

        // Enterprise, Company, Group
        Organisation = 2
    }
}