﻿namespace App.Module2.Shared.Models.Entities
{
    using System;
    using App.Core.Shared.Models.Entities;

    public class EducationOrganisation : TenantFKTimestampedAuditedRecordStatedGuidIdEntityBase
    {
        public Guid? ParentFK { get; set; }

        public SchoolEstablishmentType Type { get; set; }

        public string Key { get; set; }
        public string Description { get; set; }

        public Guid OrganisationFK { get; set; }
        public Body Organisation { get; set; }

        //FK to Principal Body
        public Guid PrincipalFK { get; set; }

        public Body Principal { get; set; }


        public string Note { get; set; }
    }
}