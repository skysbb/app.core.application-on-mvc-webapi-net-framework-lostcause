﻿namespace App.Module2.Shared.Models.Entities
{
    using App.Core.Shared.Models.Entities;

    public class BodyCategory : TenantFKTimestampedAuditedRecordStatedGuidIdEntityBase
    {
        public virtual string Text { get; set; }
    }
}