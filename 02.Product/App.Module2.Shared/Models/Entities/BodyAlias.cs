﻿namespace App.Module2.Shared.Models.Entities
{
    using System;
    using App.Core.Shared.Models;
    using App.Core.Shared.Models.Entities;

    public class BodyAlias : TenantFKTimestampedAuditedRecordStatedGuidIdEntityBase, IHasOwnerFK, IHasDisplayOrderHint,
        IHasTitle
    {
        // Display if Type=Organisation
        public string Name { get; set; }

        public string Prefix { get; set; }
        public string GivenName { get; set; }
        public string MiddleNames { get; set; }
        public string SurName { get; set; }
        public string Suffix { get; set; }

        public int DisplayOrderHint { get; set; }
        public Guid OwnerFK { get; set; }

        //Label to identify it (Alias, etc.)
        public string Title { get; set; }
    }
}