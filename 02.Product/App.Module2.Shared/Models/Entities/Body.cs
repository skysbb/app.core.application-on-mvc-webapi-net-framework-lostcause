﻿namespace App.Module2.Shared.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using App.Core.Shared.Models.Entities;

    // Should be wary of enums, but this will do for a demo

    public class Body : TenantFKTimestampedAuditedRecordStatedGuidIdEntityBase
    {
        private ICollection<BodyAlias> _aliases;
        private ICollection<BodyChannel> _channels;
        private ICollection<BodyClaim> _claims;
        private ICollection<BodyProperty> _properties;

        public BodyType Type { get; set; }

        /// <summary>
        ///     Optional unique Key.
        /// </summary>
        public string Key { get; set; }

        public Guid CategoryFK { get; set; }
        public BodyCategory Category { get; set; }

        // Display if Type=Organisation
        public string Name { get; set; }

        public string Prefix { get; set; }
        public string GivenName { get; set; }
        public string MiddleNames { get; set; }
        public string SurName { get; set; }
        public string Suffix { get; set; }

        public ICollection<BodyAlias> Aliases
        {
            get
            {
                if (this._aliases == null)
                {
                    this._aliases = new Collection<BodyAlias>();
                }
                return this._aliases;
            }
        }

        // A Body can be contacted by many different types of channels:
        public ICollection<BodyChannel> Channels
        {
            get
            {
                if (this._channels == null)
                {
                    this._channels = new Collection<BodyChannel>();
                }
                return this._channels;
            }
        }

        public ICollection<BodyProperty> Properties
        {
            get
            {
                if (this._properties == null)
                {
                    this._properties = new Collection<BodyProperty>();
                }
                return this._properties;
            }
            set => this._properties = value;
        }

        public ICollection<BodyClaim> Claims
        {
            get
            {
                if (this._claims == null)
                {
                    this._claims = new Collection<BodyClaim>();
                }
                return this._claims;
            }
            set => this._claims = value;
        }


        public string Description { get; set; }
        public string Notes { get; set; }
    }
}