﻿namespace App.Module2.Shared.Models.Entities
{
    using System;
    using App.Core.Shared.Models;
    using App.Core.Shared.Models.Entities;

    public class BodyClaim : TenantFKTimestampedAuditedRecordStatedGuidIdEntityBase, IHasOwnerFK
    {
        public string Authority { get; set; }
        public virtual string AuthoritySignature { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public Guid OwnerFK { get; set; }
    }
}