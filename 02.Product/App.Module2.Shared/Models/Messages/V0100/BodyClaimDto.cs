﻿namespace App.Module2.Shared.Models.Messages.V0100
{
    using System;
    using App.Core.Shared.Models;
    using App.Core.Shared.Models.Entities;

    public class BodyClaimDto : IHasGuidId, IHasTenantFK, IHasRecordState
    {
        public virtual Guid BodyFK { get; set; }
        public virtual string AuthorityKey { get; set; }
        public virtual string Key { get; set; }
        public virtual string Value { get; set; }
        public virtual string Signature { get; set; }
        public virtual Guid Id { get; set; }
        public virtual RecordPersistenceState RecordState { get; set; }
        public virtual Guid TenantFK { get; set; }
    }
}