﻿namespace App.Module2.Shared.Models.Messages.V0100
{
    using System;
    using App.Core.Shared.Models;
    using App.Core.Shared.Models.Entities;

    public class BodyCategoryDto : IHasGuidId, IHasTenantFK, IHasRecordState
    {
        public virtual string Text { get; set; }
        public virtual Guid Id { get; set; }
        public virtual RecordPersistenceState RecordState { get; set; }
        public virtual Guid TenantFK { get; set; }
    }
}